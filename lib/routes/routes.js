// Add the route
const Joi = require('joi')
const schema = require('../schemas/schema')
const Boom = require('boom')

module.exports = [
    {
        method: 'post',
        path: '/users/add',
        options: {
            handler: async (request, h) => {
                const { userService } = await request.services();
                const { mailService } = await request.services();

                var user = {
                    login : request.payload.schema.login,
                    password : request.payload.schema.password,
                    email : request.payload.schema.email,
                    firstname : request.payload.schema.firstname,
                    lastname : request.payload.schema.lastname,
                    company : request.payload.schema.company,
                    function : request.payload.schema.function
                }

                await mailService.sendMailLogin(user)
                await userService.postUser(user)

                return h.response('User inserted with success !').code(201)
            },
            description: 'Add new user',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    userType: 'form'
                }
            },
            validate : {
                payload : {
                    schema
                }
            }
        }
    },
    {
        method: 'post',
        path: '/users/generate',
        options: {
            handler: async (request, h) => {
                const { userService } = await request.services();

                return await userService.generateUsers();
            },
            description: 'Add new user',
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    userType: 'form'
                }
            }
        }
    },
    {
        method: 'get',
        path: '/users',
        config: {
            handler: async (request, h) => {
                const {userService} = request.services();
                return await userService.findAllUsers(request)
            },
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    userType: 'form'
                }
            }
        }
    },
    {
        method: 'get',
        path: '/users/user/{id}',
        config: {
            handler: async (request, h) => {
                const {userService} = request.services();

                var result = await userService.findUserById(request.params.id)

                if (result.length === 0) {
                    throw Boom.notFound('User not found !')
                }
                return result
            },
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    userType: 'form',
                    response: {
                        emptyStatusCode: 404
                    },
                }
            },
            validate: {
                params: {
                    id: Joi.number()
                }
            },
        }
    },
    {
        method: 'put',
        path: '/users/user/{id}',
        config: {
            handler: async (request, h) => {
                const {userService} = request.services();
                const {mailService} = request.services();

                var result = await userService.findOneAndUpdate(request.params.id,request.payload)

                var user = await userService.findUserById(request.params.id)

                if (request.payload.login !== undefined || request.payload.password !== undefined ){
                    await mailService.sendMailChangeLoginOrdPassword(user)
                }

                if(result === 0 ){
                    throw Boom.notFound('User not found !')
                }

                return result
            },
            tags: ['api'],
            description: 'Update specific user data',
            plugins: {
                'hapi-swagger': {
                    userType: 'form',
                    responses: {
                        '404': {
                            'description': 'BadRequest'
                        }
                    },
                }
            },
            validate: {
                params: {
                    id: Joi.number()
                },
                payload: {
                    login: Joi.string(),
                    password: Joi.string().alphanum().min(8),
                    email: Joi.string().email(),
                    firstname: Joi.string(),
                    lastname: Joi.string(),
                    company: Joi.string(),
                    function: Joi.string(),
                }
            },
        }
    },
    {
        method: 'delete',
        path: '/users/user/{id}',
        options: {
            handler: async (request, h) => {
                const {userService} = request.services();

                var result = await userService.delete(request.params.id)

                if(result === 0 ){
                    throw Boom.notFound('User not found !')
                }

                return h.response("User deleted with success").code(204)
            },
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    userType: 'form'
                }
            },
            description: 'Delete user',
            validate: {
                params: {
                    id: Joi.number()
                }
            }
        }
    },
    {
        method: 'post',
        path: '/auth/{login}/{password}',
        options: {
            handler: async (request, h) => {
                const {userService} = request.services();

                var result = await userService.auth(request.params)

                if (result.length === 0) {
                    return h.response('ko').code(404)
                }

                return h.response('ok').code(204)
            },
            tags: ['api'],
            plugins: {
                'hapi-swagger': {
                    userType: 'form'
                }
            },
            description: 'Authentification',
            validate: {
                params: {
                    login: Joi.string(),
                    password: Joi.string()
                }
            }
        }
    },
    {
        method: 'post',
        path: '/users/passwordLost/{email}',
        options: {
            handler: async (request, h) => {
                const { userService } = await request.services();
                const { mailService } = await request.services();

                var result = userService.findUserByMail(request.params.email)

                if(result === 0 ){
                    throw Boom.notFound('User not found !')
                }

                var newPassword = await userService.generateNewPassword(request.params.email)

                await mailService.sendMailRegeneratePwd(result,request.params.email,newPassword)

                return h.response('New mail for password regeneration !').code(201)
            },
                description: 'Add new user',
                tags: ['api'],
                plugins: {
                'hapi-swagger': {
                    userType: 'form'
                }
            },
            validate : {
                params: {
                    email: Joi.string().email()
                },
            }
        }
    },
];
