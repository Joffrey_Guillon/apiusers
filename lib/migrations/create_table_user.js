const encrypt = require('@joffrey1323/iut-encrypt')
const UserModel = require('../models/model')

exports.up = function(knex,Promise) {
    return Promise.all([
        knex.schema.createTable(UserModel.tableName(),(table) => {
            table.increments('id').primary();
            table.string('login').unique()
            table.string('password')
            table.string('email').unique()
            table.string('firstname')
            table.string('lastname')
            table.string('company')
            table.string('function')
        }).then(function () {
            return knex(UserModel.tableName()).insert({
                login: "joffreyG",
                password: encrypt.encryptInSHA1("Password1!"),
                email : "joffrey.guillon@etu.unilim.fr",
                firstname : "Joffrey",
                lastname : "Guillon",
                company: "ITI Communication",
                function : "Web Developper"
            })
        }),
    ]);
};

exports.down = function (knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists(UserModel.tableName())
    ]);
};
