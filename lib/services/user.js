'use strict';

const { Service } = require('schmervice');
const { Boom } = require('boom')
const faker = require('faker')

module.exports = class UserService extends Service {

    async initialize(){ // CALLED ON SERVER INITIALIZATION (onPreStart)

        this.users = []
    }

    async teardown(){ // CALLED ON SERVER STOP (OnPostStop)

        delete this.users
    }

    async postUser(user) {
        const { UserModel } = this.server.models();
        user.password = UserModel.encryptPassword(user.password)
        return await UserModel.query().insert(user)
    }

    async findAllUsers() {
        const { UserModel } = this.server.models();
        return await UserModel.query()
    }

    async findUserById(userId) {
            const { UserModel } = this.server.models();

            return await UserModel.query()
                .where('id',userId)
    }

    async findUserByMail(userMail) {
            const { UserModel } = this.server.models();

            return await UserModel.query()
                .where('email',userMail)
    }

    async generateNewPassword(userMail) {
        const {UserModel} = this.server.models();
        var fakePassword = faker.internet.password()
        var newPassword = {
            password: UserModel.encryptPassword(fakePassword)
        }

        await UserModel.query()
            .where('email', userMail)
            .patch(newPassword)

        return fakePassword
    }

    async findOneAndUpdate(userId,payload){
        try {
            const { UserModel } = this.server.models();

            if ('password' in payload){
                payload.password = UserModel.encryptPassword(payload.password)
            }
            return await UserModel.query()
                .where('id',userId)
                .patch(payload)
        }catch (e) {
            return await Boom.conflict(e, {statusCode: 404})
        }
    }

    async delete(userId){
        try{
            const { UserModel } = this.server.models();
            return await UserModel.query()
                .where('id',userId)
                .del()
        }catch(e)
        {
            Boom.conflict(e, {statusCode: 404})
        }
    }

    async generateUsers() {
        const { UserModel } = this.server.models();
        var tableUsers = []

        for (let i = 0; i < 100; i++) {
            var userToAdd = {
                login: faker.internet.userName(),
                password: faker.internet.password(),
                email: faker.internet.email(),
                firstname: faker.name.firstName(),
                lastname: faker.name.lastName(),
                company: faker.company.companyName(),
                function: faker.name.jobTitle()
            }
            tableUsers.push(userToAdd)
        }

        for (let i = 0; i <= tableUsers.length - 1; i++) {
            tableUsers[i].password = UserModel.encryptPassword(tableUsers[i].password)
        }
        return await UserModel.query().insert(tableUsers)
    }

    async auth(infos){
        const { UserModel } = this.server.models()

        var login = infos.login
        var password = UserModel.encryptPassword(infos.password)

        return await UserModel.query()
            .where({
                login: login,
                password: password
            })
    }
}
