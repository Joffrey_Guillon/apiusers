'use strict';

const { Service } = require('schmervice');
const nodeMailer = require('nodemailer')
const Mailgen = require('mailgen')

var mailGenerator = new Mailgen({
    theme: 'default',
    product: {
        // Appears in header & footer of e-mails
        name: 'Mailgen',
        link: 'https://mailgen.js/'
    }
});

const transporter = nodeMailer.createTransport('smtps://joffrey.guillon23@gmail.com:Joffreyguillon23@smtp.gmail.com')

    module.exports = class MailService extends Service {

    async initialize(){ // CALLED ON SERVER INITIALIZATION (onPreStart)

        this.users = []
    }

    async teardown(){ // CALLED ON SERVER STOP (OnPostStop)

        delete this.users
    }

    async sendMailLogin(user){

        var email = {
            body:{
                name:user.firstname+" "+user.lastname,
                intro: 'In this mail, your login and your password !'+
                'You can found in this mail, your login and your password !' +
                        'Login: '+user.login +" " +
                        "Password: "+user.password,
                output: ""
            }
        }

        var emailBody = mailGenerator.generate(email)

        var mailOptions = {
            from: 'joffrey.guillon23@gmail.com', // sender address
            to: user.email, // list of receivers
            subject: 'Login and password', // Subject line
            html: emailBody
        };

        await transporter.sendMail(mailOptions,await function (error, info) {
            if (error) {
                console.log(error);
            }

            console.log('Message sent :' + info.response);
        })
    }

    async sendMailChangeLoginOrdPassword(user){
        var email = {
            body:{
                name:user[0].firstname+" "+user[0].lastname,
                intro: '<h1>Your login or your password have changed ! </h1>' +
                    'Login:'+user[0].login+ '<br>' +
                    'Password:'+user[0].password
            }
        }

        var emailBody = mailGenerator.generate(email)

        var mailOptions = {
            from: 'joffrey.guillon23@gmail.com', // sender address
            to: user[0].email, // list of receivers
            subject: 'Login and password have changed', // Subject line
            html: emailBody
        };

        await transporter.sendMail(mailOptions,await function (error, info) {
            if (error) {
                console.log(error);
            }

            console.log('Message sent :' + info.response);
        })
    }

    async sendMailRegeneratePwd(user,userMail,newPassword){
        console.log(user);
        var email = {
            body:{
                name:user[0].firstname+" "+user[0].lastname,
                intro: 'New password generated:  '+ newPassword
            }
        }

        var emailBody = mailGenerator.generate(email)

        var mailOptions = {
            from: 'joffrey.guillon23@gmail.com', // sender address
            to: userMail, // list of receivers
            subject: 'New password generated', // Subject line
            html: emailBody
        };

        await transporter.sendMail(mailOptions,await function (error, info) {
            if (error) {
                console.log(error);
            }

            console.log('Message sent :' + info.response);
        })
    }
}
