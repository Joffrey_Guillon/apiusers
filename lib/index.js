'use strict';

const HauteCouture = require('haute-couture');
const Package = require('../package.json');
const Schmervice = require('schmervice');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');

exports.plugin = {
    pkg: Package,
    register: async (server, options) => {

        await server.register(Schmervice);

        await HauteCouture.using()(server, options);
    }
};
