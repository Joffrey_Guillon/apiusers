Pour démarrer le serveur :

- Ouvrir un terminal en mode administrateur
- Lancer la commande : nodemon server

Le serveur est normalement disponible à l'adresse suivante :

http://localhost:3000

Afin de pouvooir utiliser Swagger, il faut se rendre sur :

http://localhost:3000/documentation
